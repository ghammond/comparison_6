#on jt
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6b.bin --ascii bf_6b.txt --hdf5 bf_6b.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6b_wgas.bin --ascii bf_6b_wgas.txt --hdf5 bf_6b_wgas.h5
python bfpf_compare.py bf_6b pf_6b 3 2
python bfpf_compare.py bf_6b_wgas pf_6b_wgas 3 2
