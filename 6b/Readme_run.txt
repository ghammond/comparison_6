#on santana
../../bragflo -input bf_6b.inp -csd bf2_closure.dat -binary bf_6b.bin -output bf_6b.out -summary bf_6b.sum -rout bf_6b.rout
../../bragflo -input bf_6b_wgas.inp -csd bf2_closure.dat -binary bf_6b_wgas.bin -output bf_6b_wgas.out -summary bf_6b_wgas.sum -rout bf_6b_wgas.rout
pflotran -input_prefix pf_6b -output_prefix pf_6b
pflotran -input_prefix pf_6b_wgas -output_prefix pf_6b_wgas
